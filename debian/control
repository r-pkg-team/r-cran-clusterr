Source: r-cran-clusterr
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-clusterr
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-clusterr.git
Homepage: https://cran.r-project.org/package=ClusterR
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp (>= 0.12.5),
               r-cran-gmp,
               r-cran-ggplot2,
               r-cran-lifecycle,
               r-cran-rcpparmadillo (>= 0.9.1)
Testsuite: autopkgtest-pkg-r

Package: r-cran-clusterr
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R Gaussian mixture models, k-means and others
 This GNU R package provides Gaussian mixture models, k-means Mini-Batch-
 Kmeans, K-Medoids and Affinity Propagation Clustering Gaussian mixture
 models, k-means, mini-batch-kmeans, k-medoids and affinity propagation
 clustering with the option to plot, validate, predict (new data) and
 estimate the optimal number of clusters. The package takes advantage of
 'RcppArmadillo' to speed up the computationally intensive parts of the
 functions. For more information, see (i) "Clustering in an Object-
 Oriented Environment" by Anja Struyf, Mia Hubert, Peter Rousseeuw
 (1997), Journal of Statistical Software, <doi:10.18637/jss.v001.i04>;
 (ii) "Web-scale k-means clustering" by D. Sculley (2010), ACM Digital
 Library, <doi:10.1145/1772690.1772862>; (iii) "Armadillo: a template-
 based C++ library for linear algebra" by Sanderson et al (2016), The
 Journal of Open Source Software, <doi:10.21105/joss.00026>; (iv)
 "Clustering by Passing Messages Between Data Points" by Brendan J. Frey
 and Delbert Dueck, Science 16 Feb 2007: Vol. 315, Issue 5814, pp. 972-
 976, <doi:10.1126/science.1136800>.
